const _ = require('lodash');
const qs = require('qs');
const JSONAPISerializer = require('jsonapi-serializer').Serializer;

let getSchema = _.memoize(gSchema);
let shouldInclude = _.memoize(sInclude);
let hasFields = _.memoize(hFields);
let queryFields = _.memoize(qFields);

function deepFind(collection, keyToFind) {
  let found = null;
  if (_.isEmpty(collection) || !_.isPlainObject(collection)) {
    return found;
  }

  found = collection[keyToFind];
  if (found) {
    return found;
  }

  _.forEach(collection, value => {
    found = deepFind(value, keyToFind);
    if (found) {
      return false;
    }
  });

  return found;
}

function emptyResponse(meta) {
  return Object.assign({},
    meta ? meta : {}
  );
}

function isModel(model) {
  return _.has(model, 'attributes');
}

function hasRelations(model) {
  return model ?
    !_.isEmpty(model.relations) :
    false;
}

function hasAttributes(model) {
  return isModel(model) && !_.isEmpty(model.attributes);
}

function hasModels(model) {
  return !_.isEmpty(model.models);
}

function hFields(query) {
  return !_(query)
    .pickBy(
      (v, k) => k.startsWith('fields')
    )
    .isEmpty();
}

function qFields(query) {
  return _.transform(
    query,
    (result, value, key) => _.merge(
      result,
      qs.parse(`${key}=${value}`)
    ), {}
  );

  return fields;
}

function typeForAttributeWorkAround(schema) {
  return (attr, data) => {
    //This is a freaking work around for this library...
    const resource = deepFind(schema, attr);
    const type = _.isEmpty(resource) ?
      attr :
      resource.typeForAttribute(attr, data);

    return type;
  };
}

let modelAttributes = _.memoize(function(model) {
  return Object.keys(
    _.omit(
      model.toJSON({
        virtuals: true,
        shallow: true
      }),
      model.idAttribute
    )
  );
});

function getAttributeNames(model, query) {
  if (!isModel(model)) {
    return null;
  }

  // Work around:
  // model.attributes does not include the virtual properties
  let attributes = modelAttributes(model);

  if (hasFields(query)) {
    attributes = sparseFieldset(model, attributes, query);
  }

  const relations = getRelationNames(model);
  if (relations) {
    attributes.push.apply(attributes, relations);
  }

  return attributes;
}

function getRelationNames(model) {
  const relations = isModel(model) && hasRelations(model) ?
    Object.keys(model.relations) :
    null;

  return relations;
}

function sparseFieldset(model, attributes, query) {
  const fields = queryFields(query);
  const fieldsObject = fields['fields'];
  const type = model.modelType || _.kebabCase(model.tableName);
  const mask = fieldsObject[type];

  return mask ? mask.split(',') : attributes;
}

function sInclude(model, query) {
  const showIncluded = _.get(query, 'showIncluded');
  const include = _.get(query, 'include');
  return (model.includeRelationships && showIncluded !== 'false') ||
    showIncluded === 'true' ||
    !_.isEmpty(include);
}

function findRelations(model, query) {
  return _.transform(model.relations, (result, eachRel, relName) => {
    if (isModel(eachRel)) {
      result[relName] = getSchema(eachRel, query);
    } else if (hasModels(eachRel)) {
      result[relName] = _.reduce(eachRel.models,
        (res, eachModel) => {
          if (hasAttributes(eachModel)) {
            return _.merge(res, getSchema(eachModel, query));
          }
        }, {}
      );
    }
  });
}

function serializerOptions(model, query, main) {
  return Object
    .assign({}, {
      // When the query has `fields`, attributes will return a sparse field set
      attributes: getAttributeNames(model, query),
      keyForAttribute: 'camelCase',
      typeForAttribute:
        () => model.modelType || _.kebabCase(model.tableName)
    },

    main
    ? { pluralizeType: false }
    : { ref: model.idAttribute || 'id' },

    hasRelations(model)
    ? Object.assign({},
        findRelations(model, query),
        { included: shouldInclude(model, query) }
    )
    : {}
  );
}

function gSchema(model, query, main = false) {
  let data = {};

  if (isModel(model)) {
    data = serializerOptions(model, query, main);
  } else {
    data = _(model.models).reduce((res, eachModel) => {
      if (_.isEmpty(res) ||
        (hasAttributes(eachModel) && hasRelations(eachModel))
      ) {
        return _.merge(res, getSchema(eachModel, query, main));
      } else if (hasAttributes(eachModel)) {
        return getSchema(eachModel, query, main);
      } else {
        console.log(">>> THIS SHOULD NOT HAPPEN... BUT IT DID", res, '>>>', eachModel);
      }
    }, {});
  }

  return data;
}

function getSerializerSchema(model, query, meta, main) {
  console.time('schemaCreation');

  // Using the entire model instead of just the first to avoid
  // missing data issues.
  // Main attributes has a different set of options
  const schema = getSchema(model, query, main);

  // This is a freaking work around for this library...
  schema.typeForAttribute = typeForAttributeWorkAround(schema);
  if (meta) {
    schema.meta = meta;
  }

  console.timeEnd('schemaCreation');

  return schema;
}

function serializeModel(model, query, meta) {
  if (!model) {
    return emptyResponse(meta);
  }

  // Cannot depend on the first model to get entire schema.
  // if the first model does not have the related data but
  // the other models have, the related data attributes
  // for other models will not be captured.
  const first = isModel(model) ? model : model.first();
  if (!first) {
    return emptyResponse(meta);
  }

  const serializer = new JSONAPISerializer(
    first.modelType || _.kebabCase(first.tableName),
    getSerializerSchema(model, query, meta, true)
  );

  console.time('JSONAPISerializer');

  const json = serializer.serialize(
    model.toJSON({
      virtuals: true
    })
  );

  console.timeEnd('JSONAPISerializer');

  return json;
}

function toJSON(model) {
  return model ? model.toJSON() : {};
}

function toJSONAPI(model, query, meta) {
  return serialize(model, query, meta);
}

function serialize(model, query, meta) {
  if (!model) {
    return emptyResponse(meta);
  }

  const raw = _.get(query, 'raw',
    //TODO: backwards compatibility. remove till front end is not dependent
    _.get(query, 'json', 'false')
  );

  const modelQ = _.get(query, 'model');

  // TODO: Implement swagger schema.
  // At the moment, it just serialize whatever is returned.
  if (raw === 'true') {
    return model.toJSON();
  } else if (modelQ === 'true') {
    return model;
  } else {
    // JSONAPI
    return serializeModel(model, query, meta);
  }
}

function fetchOptions(model, query, txn) {
  const include = _.get(query, 'include');
  const showRelationships = _.get(
    query,
    'showRelationships',
    model.includeRelationships ? 'true' : undefined
  );
  let withRelated = null;

  if (!include && showRelationships === 'true') {
    // Load relationships by default
    let relationships = Object.keys(model.relationships);
    let additional = model.additionalRelationships;

    withRelated = _.union(
      include ? include.split(',') : [],
      relationships,
      additional
    );
  } else if (include) {
    // Load relationships by specifying the include query
    withRelated = include.split(',');
  }

  let options = Object.assign({},
    txn ? {
      transacting: txn
    } : {},
    withRelated ? {
      withRelated
    } : {}
  );

  return options;
}

module.exports = {
  serialize,
  toJSON,
  toJSONAPI,
  fetchOptions
};
