# Yus JSONAPI Serializer

A JSONAPI serializer that converts a model to JSONAPI.
At the moment, this works with bookshelf models.

## Installation

```
npm install @flamingo/yus-jsonapi-serializer --save
```

## Quick example
```
const serializer = require('@flamingo/yus-jsonapi-serializer');
const getModel = // A bookshelf model (promise)
getModel.then(model => {
  const data = serializer.toJSONAPI(model);
  ...
});
```

---

## Bookshelf Model
### Additional supported model attributes

In your bookshelf model, some attributes are supported if they're defined.

`model.modelType`: For JSONAPI compliant objects, each "resource" (model) has a `type`. If `modelType` is specified for the model, it will be used as the "resource" type. Otherwise, `tableName` is used.

`model.includeRelationships`: For JSONAPI compliant objects. If `true`, then the returned JSONAPI object will include the related "resources" even if the `include` query parameter is not specified. This can also be overridden during runtime using the `query` string, `showRelationships` and/or `showIncluded` query parameter (more on this below).

`model.additionalRelationships`: An array of nested relationships that is needed for the model.

---

## API
### `toJSONAPI(model, query, meta)`
`model`: The bookshelf model

`query`: The query object

`meta`: The meta object that will be included as additional metadata in the returned JSONAPI compliant object

##### Example

```
const serializer = require('yus-jsonapi-serializer');

const query = { ... }; // A typical query object in a http request
const meta = { ... }; // Any additional info to be added as metadata
const getModel = // A bookshelf model (promise)

getModel.then(model => {
  const data = serializer.toJSONAPI(model, query, meta);
  ...
});
```


### `toJSON(model, query)`
```
const getModel = // A bookshelf model (promise)

getModel.then(model => {
  const data = serializer.toJSON(model, query);
  ...
});

```

### `serialize(model, query, meta)`
The core function that converts the model to the desired data representation.
The parameters are the same with `toJSONAPI`.

By default, the `serialize` function returns a JSONAPI compliant object.

### `fetchOptions(model, query, txn)`

`model`: The bookshelf model

`query`: The query object (more on this below)

`txn`: The bookshelf transaction object if it is provided.

```
const serializer = require('yus-serializer');
const options = yus.fetchOptions(model, query, txn);

// This is a standard bookshelf fetch
return this.where({ ... }).fetch(options);

```

---

## Query string

#### Supported fields

`raw`: Returns raw JSON.
Example:
```
// Note the string 'true'.
// This is for consistency with the HTTP query string.
const query = {
  raw: 'true',
}
```

`include`: A comma separated string of relationships. Nested relationships are represented with the dot notation.
Example:
```
// For this step model for example,
// Include checkpoints and their steps.
// Also include the step section.
const query = {
  include: 'checkpoints.steps,section',
};
```

`showIncluded`: Used when not using `include`.

When `true`, the relationships defined in the model will be part of the JSONAPI `included` array in the returned JSONAPI object.
Example:

This will also override `model.includeRelationships` if defined in the bookshelf model.

This will just control the `included` array. The relationships will still be shown inside the `data.relationships` JSONAPI object.

```
const query = {
  showRelationships: true,
};
```

`showRelationships`: Used when not using `include`.

When `true`, the relationships defined in the model will be part of the JSONAPI `included` array in the returned JSONAPI object.
Example:

This will also override `model.includeRelationships` if defined in the bookshelf model.

This will control both `data.relationships` and `included` array.

```
const query = {
  showRelationships: true
};
```

`fields`: Used to limit the attributes returned.
Example:
```
const query = {
  'fields[step]': 'mainText,stepCode',
  'fields[section]': 'name,introduction',
};
```
